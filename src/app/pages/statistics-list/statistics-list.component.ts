import { Component, OnInit, OnDestroy } from '@angular/core';
import { StatiscticsService } from 'src/app/@core/services/statistics.service';
import { Subscription } from 'rxjs';
import { Country } from 'src/app/@core/models/country.model';

@Component({
  selector: 'app-statistics-list',
  templateUrl: './statistics-list.component.html',
  styleUrls: ['./statistics-list.component.scss']
})
export class StatisticsListComponent implements OnInit, OnDestroy {

  subscription : Subscription;
  countries : Country[];
  isLoading : boolean = true;

  constructor(
    private statsService : StatiscticsService
  ){}


  ngOnInit(){
    this.statsService.fetchCountries();
    this.subscription = this.statsService.getCountries().subscribe(res => {
      this.countries = res.data;
      this.isLoading = res.isLoading;
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
