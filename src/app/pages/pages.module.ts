import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagesRoutingModule, routedComponents} from './pages-routing.module';
import { NebularModule } from '../@shared/nebular/nebular.module';
import { NgxEchartsModule } from 'ngx-echarts';


@NgModule({
  declarations: [...routedComponents],
  imports: [
    CommonModule,
    PagesRoutingModule,
    NebularModule,
    NgxEchartsModule
  ]
})
export class PagesModule { }
