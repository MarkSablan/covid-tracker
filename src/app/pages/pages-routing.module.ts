import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages.component';
import { StatisticsListComponent } from './statistics-list/statistics-list.component';
import { WorldMapComponent } from './world-map/world-map.component';
import { CountryDetailsComponent } from './country-details/country-details.component';
import { LineChartTimelineComponent } from './country-details/line-chart-timeline/line-chart-timeline.component';
import { DeathTimelineComponent } from './country-details/death-timeline/death-timeline.component';
import { CaseTimelineComponent } from './country-details/case-timeline/case-timeline.component';


const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: '',
        component: StatisticsListComponent
      },
      {
        path:'country/:countryName',
        component: CountryDetailsComponent
      },
      {
        path: 'world-map',
        component: WorldMapComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }

export const routedComponents = [
  PagesComponent,
  StatisticsListComponent,
  WorldMapComponent,
  StatisticsListComponent,
  CountryDetailsComponent,
  LineChartTimelineComponent,
  DeathTimelineComponent,
  CaseTimelineComponent
]
