import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineChartTimelineComponent } from './line-chart-timeline.component';

describe('LineChartTimelineComponent', () => {
  let component: LineChartTimelineComponent;
  let fixture: ComponentFixture<LineChartTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineChartTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineChartTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
