import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { Subscription } from 'rxjs';
import { StatiscticsService } from 'src/app/@core/services/statistics.service';

@Component({
  selector: 'app-line-chart-timeline',
  template: `

  <nb-card>
      <nb-card-header class="d-flex justify-content-between">
        <div>Cases (Timeline)</div>
        <div *ngIf="isLoading" class="spinner-border spinner-border-sm text-primary" role="status">
          <span class="sr-only">Loading...</span>
        </div>
    </nb-card-header>
      <nb-card-body>
      <div echarts [options]="options" class="echart" (chartInit)="onChartInit($event)"></div>
      </nb-card-body>
    </nb-card>


  `,
  styles: []
})
export class LineChartTimelineComponent implements AfterViewInit, OnInit, OnDestroy {

  options: any = {};
  themeSubscription: Subscription;
  statsSubscription: Subscription;
  echartInstance : any;

  isLoading : boolean = true;

  constructor(
    private theme : NbThemeService,
    private statsService : StatiscticsService
  ) { }


  ngOnInit(): void {
    this.statsSubscription = this.statsService.getCountryHistorical().subscribe(res => {
      this.isLoading = res.isLoading;
      if(res.data != null){
        var data = Object.keys(res.data.timeline.cases).map(function(key) {
          return res.data.timeline.cases[key];
        });
        var labels = Object.keys(res.data.timeline.cases).map(function(key) {
          return key;
        });
        setTimeout(() => this.echartInstance.setOption({series: [{data : data}], xAxis : [{data : labels}]}), 1000);
      }
    });
  }

  onChartInit(eChart){
    this.echartInstance = eChart;
  }

  ngAfterViewInit(): void {
    this.themeSubscription = this.theme.getJsTheme().subscribe(config => {
      const colors: any = config.variables;
      this.options = {
        backgroundColor: colors.bg,
        color: [colors.danger],
        tooltip: {
          trigger: 'item',
          formatter: '{a} <br/>{b} : {c}',
        },
        legend: {
          left: 'left',
          data: ['Generated', 'Used'],
          textStyle: {
            color: colors.fgText,
          },
        },
        xAxis: [
          {
            type: 'category',
            data: [],
            axisTick: {
              alignWithLabel: true,
            },
            axisLine: {
              lineStyle: {
                color: colors.primary,
              },
            },
            axisLabel: {
              textStyle: {
                color: colors.fgText,
              },
            },
          },
        ],
        yAxis: [
          {
            type: 'value',
            axisLine: {
              lineStyle: {
                color: colors.primary,
              },
            },
            splitLine: {
              lineStyle: {
                color: colors.primary,
              },
            },
            axisLabel: {
              textStyle: {
                color: colors.fgText,
              },
            },
          },
        ],
        grid: {
          left: '3%',
          right: '4%',
          bottom: '3%',
          containLabel: true,
        },
        series: [
          {
            name: 'Number of Cases',
            type: 'line',
            smooth: true,
            data: [],
          },
        ],
      };
    });
  }

  ngOnDestroy(){
    this.themeSubscription.unsubscribe();
    this.statsSubscription.unsubscribe();
  }

}
