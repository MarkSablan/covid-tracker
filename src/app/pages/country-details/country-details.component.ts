import { Component, OnInit } from '@angular/core';
import { StatiscticsService } from 'src/app/@core/services/statistics.service';
import { ActivatedRoute } from '@angular/router';
import { Country } from 'src/app/@core/models/country.model';

@Component({
  selector: 'app-country-details',
  templateUrl: './country-details.component.html',
  styleUrls: ['./country-details.component.scss']
})
export class CountryDetailsComponent implements OnInit {

  country : Country;

  constructor(
    private statsService : StatiscticsService,
    private activeRoute : ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activeRoute.params.subscribe(params => {
      this.statsService.fetchHistoricalByCountry(params['countryName']);
      this.statsService.fetchCountry(params['countryName']).subscribe(res => {
        this.country = res;
      });
    });
  }

}
