import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeathTimelineComponent } from './death-timeline.component';

describe('DeathTimelineComponent', () => {
  let component: DeathTimelineComponent;
  let fixture: ComponentFixture<DeathTimelineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeathTimelineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeathTimelineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
