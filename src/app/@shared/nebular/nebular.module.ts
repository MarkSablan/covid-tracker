import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  NbLayoutModule,
  NbActionsModule,
  NbCardModule,
  NbButtonModule,
  NbInputModule,
  NbIconModule,
  NbMenuModule,
  NbUserModule,
  NbContextMenuModule,
  NbDialogModule,
  NbSelectModule,
  NbToastrModule,
  NbCheckboxModule,
  NbChatModule,
  NbListModule,
  NbBadgeModule
} from '@nebular/theme';

const nebularModules = [
  NbLayoutModule,
  NbActionsModule,
  NbCardModule,
  NbButtonModule,
  NbInputModule,
  NbIconModule,
  NbMenuModule,
  NbUserModule,
  NbContextMenuModule,
  NbSelectModule,
  NbCheckboxModule,
  NbChatModule,
  NbListModule,
  NbBadgeModule,

]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...nebularModules,
    NbDialogModule.forChild(),
    NbToastrModule.forRoot()
  ],
  exports: [
    ...nebularModules,
    NbDialogModule
  ]
})
export class NebularModule { }
