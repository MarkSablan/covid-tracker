import { Component, Input } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
    selector: 'delete-dialog',
    template: `
    <nb-card>
    <nb-card-header><p class="text-danger">Delete</p></nb-card-header>
    <nb-card-body class="modal-dialog-body">
      <div class="row">
        <div class="col">
          <p class="h5">{{ content }}</p>
          <p class="caption mt-3">
            
          </p>
        </div>
      </div>
    </nb-card-body>
    <nb-card-footer>
      <div class="row d-flex justify-content-center">
        <button nbButton status="danger" class="mr-3" (click)="confirm()" >Yes</button>
        <button nbButton status="control" (click)="close()">No</button>
      </div>
    </nb-card-footer>
  </nb-card>
    `
})

export class DeleteDialogComponent {

    @Input() data;
    @Input() content;

    constructor(
        protected ref : NbDialogRef<DeleteDialogComponent>
    ){}

    close(){
        // deny
        this.ref.close(null);
    }

    confirm(){
        // cnfirm
        this.ref.close(this.data);
    }

}