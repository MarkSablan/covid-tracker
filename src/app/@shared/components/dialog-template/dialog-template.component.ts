import { Component } from '@angular/core';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-dialog-template',
  templateUrl: './dialog-template.component.html',
  styleUrls: ['./dialog-template.component.scss']
})
export class DialogTemplateComponent{

  title : string = '';

  constructor(
    protected ref : NbDialogRef<DialogTemplateComponent>
  ) { }

  onClose(){
    this.ref.close();
  }

}
