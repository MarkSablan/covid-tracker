import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { DataTableComponent } from './data-table/data-table.component';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { DialogTemplateComponent } from './dialog-template/dialog-template.component';
import { NebularModule } from '../nebular/nebular.module';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';


const components = [
  DataTableComponent,
  DialogTemplateComponent,
  DeleteDialogComponent
]


@NgModule({
  declarations: [
    ...components
  ],
  imports: [
    CommonModule,
    Ng2SmartTableModule,
    NbEvaIconsModule,
    NebularModule,
    
  ],
  exports: [
    ...components
  ],
  entryComponents: [
    DialogTemplateComponent,
    DeleteDialogComponent,
  ]
})
export class SharedComponentsModule { }
