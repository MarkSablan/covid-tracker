import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'data-table',
  template: `
    <ng2-smart-table
      [settings]="settings"
      [source]="source"
      (edit)="onEdit($event)"
      (delete)="onDelete($event)"
      (userRowSelect)="onUserRowSelect($event)"
    ></ng2-smart-table>`,
  styles: []
})
export class DataTableComponent implements OnInit {

  @Input() source;
  @Input() settings;

  @Output() rowSelect = new EventEmitter;
  @Output() delete = new EventEmitter;
  @Output() edit = new EventEmitter;

  constructor() { }

  ngOnInit() {
  }

  onUserRowSelect($event){
    this.rowSelect.emit($event.data);
  }

  onEdit($event){
    this.edit.emit($event.data);
  }

  onDelete($event){
    this.delete.emit($event.data);
  }

}
