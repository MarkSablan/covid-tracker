import { GenericInterface } from 'src/app/@shared/models/generic-interface.model';

export interface CountryHistorical extends GenericInterface{
  data : Historical
}

export interface Historical {
  country: string
  timeline: Timeline
}

export interface Timeline{
  cases: object
  deaths: object
}

