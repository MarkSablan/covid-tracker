import { GenericInterface } from 'src/app/@shared/models/generic-interface.model';

export interface Countries extends GenericInterface{
  data: Country[],
}

export interface Country{
  country: string
  cases : number
  countryInfo: CountryInfo
  todayCases: number
  deaths: number
  todayDeaths: number
  recovered: number
  active: number
  critical: number
  casesPerOneMillion: number
  deathsPerOneMillion: number
}

export interface CountryInfo{
  _id: number
  lat: number
  long: number
  flag: string
  iso3: string
  iso2: string
}
