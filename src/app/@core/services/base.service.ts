import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpErrorResponse } from "@angular/common/http";
import { Observable, BehaviorSubject, throwError } from "rxjs";
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: "root"
})
export class BaseService {


  private url : string =  environment.apiUrl;

  constructor(public http: HttpClient) {}

  getHeader(withToken: boolean) {
    let header = {
      headers: new HttpHeaders({
        "Content-type": "application/json"
      })
    };
    if (withToken) {
      header = {
        headers: new HttpHeaders({
          "Content-type": "application/json",
          "Authorization": "Bearer " + localStorage.getItem("token")
        })
      };
    }
    return header;
  }

  setData(list: BehaviorSubject<any>, newList: any) {
    const data = list.value;
    data.data = newList;
    list.next(data);
  }

  setLoading(list: BehaviorSubject<any>) {
    const data = list.value;
    data.isLoading = true;
    list.next(data);
  }

  unsetLoading(list: BehaviorSubject<any>) {
    const data = list.value;
    data.isLoading = false;
    list.next(data);
  }

  $get(api: string, withToken: boolean = true): Observable<any> {
    return this.http.get(this.url + api, this.getHeader(withToken));
  }

  $post(api: string, body?: any, withToken: boolean = true): Observable<any> {
    return this.http.post(this.url + api, body, this.getHeader(withToken));
  }

  $delete(api: string, withToken: boolean = true): Observable<any> {
    return this.http.delete(this.url + api, this.getHeader(withToken));
  }

  $put(api : string, body? : any, withToken : boolean = true): Observable<any>{
    return this.http.put(this.url + api, body, this.getHeader(withToken));
  }

  $postFromOtherUrl(
    url: string,
    body?: any,
    withToken: boolean = true
  ): Observable<any> {
    return this.http.post(url, body, this.getHeader(withToken));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
}
