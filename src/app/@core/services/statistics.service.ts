import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BaseService } from './base.service';
import { BehaviorSubject } from 'rxjs';
import { Countries } from '../models/country.model';
import { CountryHistorical } from '../models/country-historycal.model';

@Injectable({providedIn: 'root'})

export class StatiscticsService extends BaseService{

  private _countries : BehaviorSubject<Countries> = new BehaviorSubject<Countries>({data : null, isLoading : true});
  private _coutryHistorical : BehaviorSubject<CountryHistorical> = new BehaviorSubject<CountryHistorical>({data : null, isLoading: true});

  constructor(public http : HttpClient) {
    super(http);
  }


  getCountries(){
    return this._countries.asObservable();
  }

  getCountryHistorical(){
    return this._coutryHistorical.asObservable();
  }

  fetchAll(){
    this.$get('/v2/all', false).subscribe(res => {

    });
  }

  fetchCountries(){
    this.setLoading(this._countries);
    this.$get('/v2/countries', false).subscribe(res => {
      this._countries.next({isLoading : false, data : res});
    });
  }

  fetchCountry(country : string){
    return this.$get('/v2/countries/' + country, false);
  }

  fetchStates(){
    this.$get('/v2/states', false).subscribe(console.log);
  }

  fetchHistorical(){
    this.$get('/v2/historical', false).subscribe(console.log);
  }

  fetchHistoricalByCountry(country : string){
    this.setLoading(this._coutryHistorical);
    this.$get('/v2/historical/' + country, false).subscribe(res => {
      this._coutryHistorical.next({data : res, isLoading : false});
    });
  }

}
